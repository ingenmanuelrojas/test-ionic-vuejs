import Vue from 'vue'
import App from './App.vue'
import router from './router'

import Ionic from "@ionic/vue"; //Importar Ionic
import '@ionic/core/css/ionic.bundle.css'; //Importar los css de Ionic
Vue.use(Ionic);

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

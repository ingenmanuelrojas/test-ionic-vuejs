import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Componentes from '../views/Componentes.vue'
import Formulario from '../views/Formulario.vue'

Vue.use(VueRouter)

  const routes = [
    {
      path: "/",
      name: "Home",
      component: Home,
    },
    {
      path: "/detail",
      name: "detail",
      component: () => import("../views/Detail.vue"),
      props: true,
    },
    {
      path: "/componentes",
      name: "componentes",
      component: Componentes,
    },
    {
      path: "/formulario",
      name: "formulario",
      component: Formulario,
    },
    {
      path: "/about",
      name: "About",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "../views/About.vue"),
    },
  ];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
